package com.teamc.init;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;

public class ConnectionAndDocument {

    public static HttpURLConnection getConnection(URL url) throws IOException {
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("proxy-gw.raiffeisen.ru", 8080));
        HttpURLConnection connection = (HttpURLConnection) url.openConnection(proxy);
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Accept", "application/xml");
        return connection;
    }
}
